{ nixOrg ? import <nix-org> {}
} :
with nixOrg;
org "example"
{
  base = config "base" {};
  dev = system "dev"
  {
    include = [ "base" ];

    test = deploy "libvirtd" {};
    beta = deploy "ec2"
    {
      ephemeral = true;
      persistent = true;
    };
    prod = deploy "ec2"
    {
      persistent = true;
    };
  };
};
