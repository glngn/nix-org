# nix-org

Hierarchical organizations defined by:

- shared data
- authentication and authorization
- public and private services

Each organization can be viewed as module with secret, private, and public accessible services. Where

- secret: subset of organization
- private: within organization only
- public: outside of organization

Each organization repo defines:

- accessible data from defined public or named private data sets
    - assumes small and blob-like
    - datasets can be stored separate from config repo
    - optionally distributed via nix
- modules to

## Implementation

- nixos modules
- overloads of existing modules?
- nixos configuration
- nixpkgs configuration
- expose interface commands and nix-* command overrides
- packages visible to user automatically integrate:
    - only active if git repo exists and added by org admin.
    - configuration of nixpkgs in user context
        - along with other pkg sets
    - list of packages attributes in env
    - nix-env is hidden or remapped to operations on above
    - nixpkgs.config defined in NixOS configuration.

The modules that are composed to form a nix-org based system have

1. an exported store representation containing exactly:

- nix/$ORG_NAME/default.nix
- nixos/$ORG_NAME/default.nix
- nixpkgs-overlays/$ORG_NAME/*.nix
- nix-org/$ORG_NAME/dependencies/*.nix

2. a private source representation containing at least:

- default.nix built using <nix-org/lib/module.nix>
- lib/
    - default.nix - optional. Extends lib provided to self and orgs including
    - private/default.nix - optional. Extends lib privided to self.
- modules/
    - default.nix - optional. Extends modules available in this org and including.
    - private/default.nix - optional. Extends modeuls available to nodes in org.
- network/
    - default.nix
- users/*.nix
- pkgs
    - *.nix
- dependencies/*

## Security levels

Partitioned into

- read access to service providing data class
- write access to service persisting data class
- access to read only secrets. authorization for read to a data class
- access to read-write secrets authorization for write to a data class

H: finer grained partitioning is service specific.

Secrets are partitioned seperate from services and have associated generator services. Which, as implied above,
have seperate authorization for access.

# references

- http://www.scala-sbt.org/0.13.5/docs/Launcher/Configuration.html
- https://github.com/cessationoftime/Sbtix
- https://github.com/scopt/scopt
- http://www.herongyang.com/crypto/openssl_verify.html
- http://www.freebsdmadeeasy.com/tutorials/freebsd/create-a-ca-with-openssl.php
- https://en.wikipedia.org/wiki/X.509#Certificate_chains_and_cross-certification

- /home/coconnor/Development/origin/pkg/oc/clusterup/run_self_hosted.go

# user stories

## Developer

- command "dev" defaults to development environment of project implied by current working dir
- command family "dev-PROJECT" target development environment of "PROJECT".
    - automatically created for all projects of services the user has audit access to
- command family "ENV-PROJECT" target ENV environment of "PROJECT".
    - automatically created for all environments and projects of services the user has write access to
    - note: no actual separation of "beta" from "prod". Only diff is secrets and integration path.

# Scratch

~~~
from router: 2601:602:9700:f0fc::/64
Prefix/L:  fd
Global ID:  2e1bf4effb
Subnet ID:  4300
Combine/CID:  fd2e:1bf4:effb:4300::/64
IPv6 addresses:  fd2e:1bf4:effb:4300::/64:XXXX:XXXX:XXXX:XXXX
Start Range:  fd2e:1bf4:effb:4300:0:0:0:0
End Range:  fd2e:1bf4:effb:4300:ffff:ffff:ffff:ffff
No. of hosts:  18446744073709551616
~~~

# Agh Dev Cluster

~~~
oc cluster up --public-hostname=agh --routing-suffix=agh.dev --server-loglevel=4 --loglevel=4 --host-data-dir=/var/lib/origin/openshift.local.etcd --logging --use-existing-config
oc login -u system:admin
oc project default
oc new-project glngn
oc adm groups new glngn-ops developer
oc adm policy add-role-to-group admin glngn-ops
oc new-app centos/nginx-112-centos7~https://github.com/coreyoconnor/nginx-ex.git --name=static-www
~~~
