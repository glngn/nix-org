self: super: {
  ccminer-cryptonight = self.callPackage (import dependencies/ccminer-cryptonight.nix) { };

  siac = self.callPackage (import dependencies/siac.nix) { };
  siad = self.callPackage (import dependencies/siad.nix) { };
  sia-gpu-miner = self.callPackage (import dependencies/sia-gpu-miner.nix) { };

  xmr-stak-cpu = self.callPackage (import dependencies/xmr-stak-cpu.nix) { };
  xmr-stak-cpu-autoconf = self.writeShellScriptBin "xmr-stak-cpu-autoconf" ''
    base_conf="$1"
    final_conf=$(mktemp)
    ${self.xmr-stak-cpu}/bin/xmr-stak-cpu $base_conf | ${self.gawk}/bin/awk '/BEGIN/{p=1;next} p&&/END/{p=0} p' > $final_conf
    ${self.coreutils}/bin/tail -n+2 "$base_conf" >> $final_conf
    exec ${self.xmr-stak-cpu}/bin/xmr-stak-cpu $final_conf
  '';
}
