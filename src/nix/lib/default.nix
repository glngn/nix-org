{ nixpkgs-config, nixpkgs, src, ... }:
let
  pkgs = import nixpkgs {
    config = import ./../nixpkgs-config.nix;
  };
in
with pkgs.lib;
let
  config =
  {
    inherit nixpkgs nixpkgs-config;
  };
  dsl = import ./dsl { inherit pkgs config; };
  dev = import ./dev { inherit pkgs config dsl; };
  local = import ./local { inherit pkgs config dsl; };
  persistent = import ./persistent { inherit pkgs config dsl; };
in {
   inherit config;
   inherit dev;
   inherit dsl;
   inherit local;
   inherit persistent;
   inherit pkgs;
}
