{ pkgs, config, dsl, ... }: def:
{
  env = stdenv.mkDerivation rec {
    name = "nix-org-env";
    shellHook = ''
      alias cls=clear
    '';
    CLANG_PATH = pkgs.clang + "/bin/clang";
    CLANGPP_PATH = pkgs.clang + "/bin/clang++";
    buildInputs = with pkgs; [
      stdenv
      sbt
      openjdk
      boehmgc
      libunwind
      re2
      clang
      zlib
    ];
  };
}
