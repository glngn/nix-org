{ pkgs, config, dsl }:
with dsl;
rec {
  DeployExpr = mkExpr "deployment";

  Deploy = implName: deployAttrs:
    deployAttrs // DeployExpr // { impl = implName; };

  buildDeployEnv = systemDecl: givenDecl:
  let
    deployDecl =
    {
      impl = givenDecl.name;
      isEphemeral = true;
      syncDeploymentState = false;
    } // givenDecl;

    deployName = deployDecl.name;
    systemName = systemDecl.name;

    deployNetworkDecl = NetworkDecl (systemDecl.impl + "-" + deployDecl.impl + ".nix");
    deployNetworkDecls = NetworkDecls (systemDecl.networkDecls ++ [deployNetworkDecl]);

    evalNetworkDecls = networkDecls: import "${pkgs.nixops}/share/nix/nixops/eval-machine-info.nix"
    {
      networkExprs = networkDecls.physicals;
      uuid = "GLOBAL";
      deploymentName = deployName;
      args = {};
    };

    # initial eval
    stage0Eval = evalNetworkDecls deployNetworkDecls;

    # EC2
    ec2DefaultsDecl = NetworkDecl ./../modules/ec2-network-defaults.nix;
    ec2NetworkDecls = deployNetworkDecls.add ec2DefaultsDecl;
    ec2Eval = evalNetworkDecls ec2NetworkDecls;

    nodeArtifacts = nodeName: node:
      let
        ec2NodeEval = getAttr nodeName ec2Eval.nodes;
        ec2Artifacts =
          let
            ami = import ./make-ami-image.nix
            {
              inherit pkgs;
              node = ec2NodeEval;
            };
            nodeRegion = node.config.deployment.ec2.region;
            amiUploader = pkgs.substituteAll
            {
              name = "${systemName}-${deployName}-${nodeName}-ami-uploader";
              src = ./bin/ami-uploader.sh;
              isExecutable = true;

              inherit ami nodeRegion;
              amiName = "${systemName}-${deployName}-${nodeName}";

              path = makeBinPath
              [
                pkgs.awscli
                pkgs.coreutils
                pkgs.curl
                pkgs.ec2_ami_tools
                pkgs.ec2_api_tools
                pkgs.gnutar
                pkgs.gzip
                pkgs.jq
                pkgs.qemu
                pkgs.utillinux
              ];
              inherit nixpkgsVersion;
              inherit (pkgs) bash;
            };
          in optionalAttrs (node.config.deployment.targetEnv == "ec2")
          {
            inherit ami;
            ami-uploader = amiUploader;
          };
      in ec2Artifacts // {
        inherit (node) config;
      };
    nodes = mapAttrs nodeArtifacts stage0Eval.nodes;
  in rec {
    inherit deployDecl deployName systemName;
    inherit deployNetworkDecl deployNetworkDecls;
    inherit nodes;

    ops-env-init = pkgs.substituteAll
    {
      name = "${systemName}-${deployName}-ops-env-init";
      src = ./bin/ops-env-init.sh;
      isExecutable = true;

      inherit (pkgs) bash nixops nix;

      networkExprsPhysicals = deployNetworkDecls.physicals;
      networkExprsLogicals = deployNetworkDecls.logicals;
      networkExprsOpsExports = deployNetworkDecls.opsExports;
      isEphemeral = if deployDecl.isEphemeral then "true" else "false";
      syncDeploymentState = if deployDecl.syncDeploymentState then "true" else "false";
      inherit systemName deployName;
    };
    opsEnvInit = ops-env-init;

    ops-lib = pkgs.substituteAll
    {
      name = "${systemName}-${deployName}-ops-lib";
      src = ./bin/ops-lib.sh;
      isExecutable = true;
      inherit opsEnvInit;
    };
    opsLib = ops-lib;

    ops = pkgs.substituteAll
    {
      name = "${systemName}-${deployName}-ops";
      src = ./bin/ops.sh;
      isExecutable = true;

      inherit (pkgs) bash;
      inherit deployName systemName;
      inherit opsEnvInit opsLib;
    };
  };
}
