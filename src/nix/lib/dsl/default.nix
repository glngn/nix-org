{ pkgs, config, ... }:
with pkgs.lib;
let
  modules =
  [
    ./types.nix
    ./networks.nix
    ./deploys.nix
    ./systems.nix
    ./orgs.nix
  ];
  mergeModules = dsl:
    let
      imported = map (module: import module { inherit pkgs config dsl; }) modules;
      merge = m0: m1: m0 // m1;
      merged = foldr merge pkgs.lib imported;
    in merged;
in fix mergeModules
