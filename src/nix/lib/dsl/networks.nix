{ pkgs, config, dsl }:
with dsl;
rec {
  # all modules must be specified as paths to network expr files.
  # this is annoying and an aspect I'd like to remove from nixops
  # for now tho...
  NetworkDecl = logicalOrPhysical: rec
  {
    physical = if (types.path.check logicalOrPhysical)
              then logicalOrPhysical
              else builtins.toPath(config.baseSrcPath + logical);
    logical = if (types.path.check logicalOrPhysical)
              then removePrefix config.baseSrcPath logicalOrPhysical
              else logicalOrPhysical;
    # TODO: hard coded "src"
    # TODO: hidden interface with the ops scripts WRT PROJECT_DIR/src
    opsExport = "\'$PROJECT_DIR/src/${logical}\'";
  };

  NetworkDecls = networkDecls:
  {
    physicals = map (networkDecl: networkDecl.physical) networkDecls;
    logicals = map (networkDecl: networkDecl.logical) networkDecls;
    opsExports = concatStringsSep " " (map (networkDecl: networkDecl.opsExport) networkDecls);

    add = networkDecl: NetworkDecls (networkDecls ++ [networkDecl]);
  };
}
