{ pkgs, config, dsl }:
with dsl;
{
  SystemExpr = mkExpr "system";

  System = implName: systemAttrs:
    systemAttrs // SystemExpr // { impl = implName; };

  buildSystemEnv = givenDecl:
    let
      systemDecl = givenDecl // rec
      {
        systemNetworkDecl = NetworkDecl (givenDecl.impl + ".nix");
        systemBaseNetworkDecl = NetworkDecl "system-base.nix";
        networkDecls = [ systemNetworkDecl systemBaseNetworkDecl ];
      };
    in
  {
    inherit systemDecl;
    buildDeployEnv = buildDeployEnv systemDecl;
  };

  unfoldSystem = systemName: systemAttrs:
    let
      systemEnv = buildSystemEnv (systemAttrs // { name = systemName; });
      unfoldDeploy = deployName: deployAttrs:
        systemEnv.buildDeployEnv (deployAttrs // { name = deployName; });
    in unfoldExpr DeployExpr
                  unfoldDeploy
                  systemEnv.systemDecl;
}
