{ pkgs, config, dsl }:
with dsl;
{
  mkExpr = exprName: { _expr = exprName; };

  unfoldExpr = exprType: expandExpr: attrs:
    let
      passthrough = attrName: attrValue:
        if (matchAttrs exprType attrValue)
        then (expandExpr attrName attrValue)
        else attrValue;
    in mapAttrs passthrough attrs;
}
