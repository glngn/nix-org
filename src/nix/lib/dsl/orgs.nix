{ pkgs, config, dsl }:
with dsl;
{
  OrgExpr = mkExpr "org";

  Org = orgName: orgAttrs:
    orgAttrs // OrgExpr // { name = orgName; };

  unfoldOrg = orgAttrs:
    unfoldExpr SystemExpr
               unfoldSystem
               orgAttrs;

  org = orgName: orgAttrs: unfoldOrg (Org orgName orgAttrs);
}
