{ nixpkgsSrc
, overlays ? []
}:
let
  nixpkgs-config = import ./nixpkgs/config.nix;
  nixpkgs-overlays = overlays ++ [
    (import ./nixpkgs/to-be-moved.nix)
  ];
  pkgs = import "${nixpkgsSrc}/pkgs/top-level/impure.nix" {
    config = nixpkgs-config;
    overlays = nixpkgs-overlays;
  };
in
with pkgs.lib;
let
  config = {
    nixpkgs = {
      src = nixpkgs;
      config = nixpkgs-config;
    };
  };
in {
   inherit config;
   inherit pkgs;
}
