#!/bin/bash

# move to home
mkdir -p .cache
export CACHE_DIR="$PROJECT_DIR/.cache"

mkdir -p "$CACHE_DIR/bin/"
mkdir -p "$CACHE_DIR/builds/"
export DEV_CACHE_PATH="$CACHE_DIR/dev"
export DEV_BIN_PATH="$CACHE_DIR/dev/bin/dev-out"
export SBT_PATH="$CACHE_DIR/sbt"
export SBT_BIN_PATH="$SBT_PATH/bin/sbt"
export SBTIX_PATH="$CACHE_DIR/sbtix"
export SBTIX_BIN_PATH="$SBTIX_PATH/bin/sbtix"
