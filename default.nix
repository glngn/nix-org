import ./src/nix {
  devSrc = ./dependencies/dev;
  nixpkgsSrc = ./dependencies/nixpkgs;
  sbtixSrc = ./dependencies/sbtix;
}
